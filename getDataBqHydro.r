
df<-rbanqhydro.get("O0484010","01/01/1988", "00:00", "05/04/2017", "23:59")

rbanqhydro.get <- function (station, DateDeb, HeureDeb, DateFin, HeureFin, procedure = "QTVAR", url="http://www.hydro.eaufrance.fr") {

  require("httr")

  # Formulaire de sélection des stations
  form0<- list(
    cmd = "filtrer",
    consulte = "rechercher",
    code_station = "",
    cours_d_eau = "",
    commune = "",
    departement = "",
    bassin_hydrographique = "",
    station_en_service = "1",
    station_hydrologique = "1",
    btnValider = "Visualiser"
  )
  form0[["station[]"]] = station

  url.selection = paste(url,"selection.php", sep = "/")

  res <- POST(
    url.selection,
    body = form0, encode = "form", verbose()
  )

  # Formulaire de sélection de la variable
  form1 <- list(
    categorie = "rechercher",
    procedure = procedure
  )
  form1[["station[]"]] =  station

  url.procedure = paste(url,"presentation/procedure.php", sep = "/")
  res <- POST(
    url.procedure,
    body = form1, encode = "form", verbose()
  )

  # Formulaire de sélection de la date
  form2 <- list(
    procedure = procedure,
    affichage = 2,
    echelle = 1,
    date1 = DateDeb,
    heure1 = HeureDeb,
    date2 = DateFin,
    heure2 = HeureFin,
    precision = "00",
    btnValider = "Valider"
  )

  res <- POST(
    url.procedure,
    body = form2, encode = "form", verbose()
  )

  PackageRequire("XML")
  # On récupère le dataframe du 3ème tableau de la page
  df = readHTMLTable(content(res, type="text/plain", encoding="Latin1"))[[3]]

  return(df)

}




################################################################################
#' Test la présence d'un package, le télécharge au besoin et le charge.
#' Le programme est stoppé en cas d'échec.
#' @param x Chaîne de caractère avec le nom du package à charger
#' @url http://stackoverflow.com/questions/9341635/how-can-i-check-for-installed-r-packages-before-running-install-packages
#' @date 31/07/2014
################################################################################
PackageRequire <- function(x)
{
  if (!require(x,character.only = TRUE)) {
    install.packages(x,dep=TRUE,repos="http://cran.r-project.org")
  }
  if(!require(x,character.only = TRUE)) {
    stop("Package not found")
  }
}
