### Read stations list from datafile with starting year
### Loop through each station name and scrap data from online
inputStations <- read.table('codes_station_startY.csv', sep=";", header=TRUE,stringsAsFactors = FALSE)
dtend <- "31/12/2018"
YearEnd <- "2018"

for (i in 1:length(inputStations)) {
  stationNb <- inputStations[i,1]
  dtini <- paste0('01/01/', inputStations[i,2])

  filename  <- paste0('qtfix1_',stationNb,'_', inputStations[i,2], '_',YearEnd,'.txt'  )

  dd <- bdhydroloop(stationNb,dtini, "00:00", dtend, "00:00")

  ## write data to output file
  write.table(dd, file = filename, quote = FALSE, sep = ";",
  eol = "\n", na = "NA", dec = ".", row.names = FALSE,
  col.names = TRUE, qmethod = c("escape", "double"),
  fileEncoding = "")
}

### Functions -

## loop through years
bdhydroloop <- function(station, DateDeb, HeureDeb, DateFin, HeureFin, procedure = 'QTFIX') {
  ## This functions calls hydro.eaufrance.fr to retrieve a dataset for one station
  ## it loops over the specified years and splits the website call over by 6 months for each years.
  ## 06/2019 - Vivien Hakoun - v.hakoun@brgm.fr

  YearDeb <- as.numeric(strsplit(DateDeb, "/")[[1]][3])
  YearEnd <- as.numeric(strsplit(DateFin, "/")[[1]][3])
  dyear <- YearEnd - YearDeb

  dfx <- data.frame("v1"=character(), "v2"=numeric(), "v3"=factor() )
  colnames(dfx) <- c("Date","Q (m3/s)","V")

  for (i in 0:dyear) {
    ## request over the first 6 months
    dts1 <- paste0('01/01/',YearDeb+i)## year increment
    dte1 <- paste0('01/07/',YearDeb+i)## year increment
    dtmp1 <- rbanqhydro.getqtfix (station, dts1, HeureDeb, dte1, HeureFin, procedure = procedure, url="http://www.hydro.eaufrance.fr")

    dts2 <- paste0('01/07/',YearDeb+i)## year increment
    dte2 <- paste0('01/01/',YearDeb+i+1)## year increment
    ## request over the second 6 months
    dtmp2 <- rbanqhydro.getqtfix (station, dts2, HeureDeb, dte2, HeureFin, procedure = procedure, url="http://www.hydro.eaufrance.fr")

    ## bind all requests to df
    dfx <- rbind(dfx, dtmp1, dtmp2)
  }

  return(dfx)
}


rbanqhydro.getqtfix <- function (station, DateDeb, HeureDeb, DateFin, HeureFin, procedure = "QTFIX", url="http://www.hydro.eaufrance.fr") {

  require("httr")

  # Formulaire de sélection des stations
  form0<- list(
    cmd = "filtrer",
    consulte = "rechercher",
    code_station = "",
    cours_d_eau = "",
    commune = "",
    departement = "",
    bassin_hydrographique = "",
    station_en_service = "1",
    station_hydrologique = "1",
    btnValider = "Visualiser"
  )
  form0[["station[]"]] = station

  url.selection = paste(url,"selection.php", sep = "/")

  res <- POST(
    url.selection,
    body = form0, encode = "form", verbose()
  )

  # Formulaire de sélection de la variable
  form1 <- list(
    categorie = "rechercher",
    procedure = procedure
  )
  form1[["station[]"]] =  station

  url.procedure = paste(url,"presentation/procedure.php", sep = "/")
  res <- POST(
    url.procedure,
    body = form1, encode = "form", verbose()
  )

  # Formulaire de sélection de la date ## QTFIX
  form2 <- list(
    procedure = procedure,
    affichage = 2,
    echelle = 1,
    date1 = DateDeb,
    heure1 = HeureDeb,
    date2 = DateFin,
    heure2 = HeureFin,
    pastmps = "1",
    btnValider = "Valider"
  )

  res <- POST(
    url.procedure,
    body = form2, encode = "form", verbose()
  )

  PackageRequire("XML")
  # Read 3rd table on webpage

  df = readHTMLTable(content(res, type="text/plain", encoding="Latin1"))
  if (length(df) >= 3) {
    df <- df[[3]]
  } else {
    df <- data.frame("v1"=character(), "v2"=numeric(), "v3"=factor() )
    colnames(df) <- c("Date","Q (m3/s)","V")
  }

  return(df)

}




################################################################################
#' Test la présence d'un package, le télécharge au besoin et le charge.
#' Le programme est stoppé en cas d'échec.
#' @param x Chaîne de caractère avec le nom du package à charger
#' @url http://stackoverflow.com/questions/9341635/how-can-i-check-for-installed-r-packages-before-running-install-packages
#' @date 31/07/2014
################################################################################
PackageRequire <- function(x)
{
  if (!require(x,character.only = TRUE)) {
    install.packages(x,dep=TRUE,repos="http://cran.r-project.org")
  }
  if(!require(x,character.only = TRUE)) {
    stop("Package not found")
  }
}
